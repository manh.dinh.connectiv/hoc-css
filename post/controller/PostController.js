var shortid = require('shortid');
var db = require('../db');

var posts = db.get('posts');

module.exports.getPost = function(req, res) {
    res.render('post/index', {
        posts : posts.value()
    })
}

module.exports.createPost = function(req, res) {
    res.render('post/create')
}

module.exports.store = function(req, res) {
    req.body.id = shortid.generate();
    posts.push(req.body).write();
    res.redirect('/post/');
}

module.exports.search = function(req, res) {
   var searchValue = req.query.searchValue;
   var searchPost = posts.value().filter( function (post) {
        return post.name.toLowerCase().indexOf(searchValue.toLowerCase()) != -1;
   });
   res.render('post/index', 
   {
    posts : searchPost,
    searchValue : searchValue
    })
}

module.exports.show = function (req, res) {
    var postId = req.params.id      ;
    var getPost = posts.value().filter( function (post) {
        return post.id == postId;
    })
    res.render('post/view',
        {
            posts : getPost
        }
    )
}

module.exports.update = function (req, res) {
    var name = req.body.name;
    var postId = req.params.id;
    var post =  posts.find({id : postId});
   
    if(post.value().id == postId) {
        post.assign({ name: name})
        .write()
    }
    res.redirect('/post');
}