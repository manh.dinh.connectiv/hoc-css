const express = require('express');
const app = express();
const port = 3000;
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

app.set('view engine', 'pug');
app.set('views', './views');

var postRoute = require('./routes/routePost');
app.use('/post/', postRoute);


app.listen(port, () => {
    console.log('Example app listening at http://localhost:' + port)
})