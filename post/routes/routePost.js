var express = require('express');
var router = express.Router();

var postController = require('../controller/PostController');

router.get('/', postController.getPost);

router.get('/create', postController.createPost)

router.post('/create', postController.store)

router.get('/search', postController.search)

router.get('/:id', postController.show)

router.post('/update/:id', postController.update)

module.exports = router;