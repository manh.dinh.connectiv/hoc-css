require('dotenv').config();
var express = require('express');
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')

const mongoose = require('mongoose');

mongoose.connect(process.env.MONGO_URL);




var app = express();
app.use(bodyParser.urlencoded({
    extended: true
 }));
 
app.use(bodyParser.json());
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

var apiProduct = require('./api/routers/product.router');
var sessionMiddleware = require('./middleware/session.middleware');
var sessionTatolCart = require('./middleware/tatolCart.middleware');
var middleware = require('./middleware/auth.middleare');1

app.use(express.static('public'))
app.use(cookieParser(process.env.SESSION_SECRET))
app.use(sessionMiddleware)
app.use(sessionTatolCart)

var auth = require('./routes/auth.route')
var  userRouter = require('./routes/user.route');
var product = require('./routes/product.route');
var cartRoute = require('./routes/cart.route'); 
var registration = require('./routes/registration.route')

var db = require('./db');
const { use } = require('./api/routers/product.router');


app.set('view engine', 'pug')
app.set('views', './views')

var port = 3000;


app.get('/', function(req, res) {
    res.render('index', {
        hello : 'manh'
    });
});

app.use('/api/products', apiProduct)
app.use('/login', auth);
app.use('/user/', userRouter);
app.use('/product', product);
app.use('/cart', cartRoute);
app.use('/registration', registration);

app.listen(port, function() {
    console.log('Server listening on posr ' +  port);
});