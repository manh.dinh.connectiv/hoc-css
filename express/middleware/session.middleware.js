var shortid = require('shortid');
var Session = require('../models/session.model')

var db = require('../db');

module.exports = async function(req, res, next) {
    if(!req.signedCookies.sessionId) {
        var sessionId = shortid.generate();
        res.cookie('sessionId', sessionId, {
            signed: true
        });
    //    db.get('session').push({
    //        id: sessionId,
    //    }).write();
        await Session.create({_id: sessionId, cart: {}});
    }
    
    next();
}