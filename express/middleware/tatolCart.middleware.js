
var Session = require('../models/session.model')

module.exports = async (req, res, next) => {
    var sessionId = req.signedCookies.sessionId;
    if (sessionId)  {
        var cart = await Session.find({_id: sessionId}).exec();
        if(cart[0].cart) {
            res.locals.cart = Object.keys(cart[0].cart).length;
            next();
            return;
        }
    }
   next();
   return;
}