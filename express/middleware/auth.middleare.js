var db = require('../db');
var User = require('../models/user.model');

module.exports.authMiddleare = async function(req, res, next) {
    if (!req.signedCookies.userId) {
        res.redirect('/login');
        return;
    }
    var user = await User.findOne({_id : req.signedCookies.userId}).exec();
    if(!user) {
        res.redirect('login');
        return;
    }
    res.locals.user = user;
    next();
}