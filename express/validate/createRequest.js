module.exports.createMiddleware = function (req, res , next) {

    var errors = [];
    if(!req.body.name){
        errors.push('Name is required');
    }
    if(!req.body.phone){
        errors.push('phone is required');
    }
    if(errors.length > 0) {
        // console.log('err', errors);
        // console.log('value', req.body);
        res.render('user/create', {
            errors: errors,
            values: req.body
        })
        return;
    }
    next();
};