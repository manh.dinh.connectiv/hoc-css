var express = require('express');
var controller = require('../controller/Cart.controller');


var route = express.Router();

route.get('/add/:productId', controller.appToCart)

module.exports = route;