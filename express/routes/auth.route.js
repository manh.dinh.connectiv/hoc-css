var express = require('express');

var controller = require('../controller/LoginController');
var middleware = require('../middleware/auth.middleare');

var router = express.Router();

router.get('/', controller.login);

router.post('/',  controller.postLogin);

module.exports = router;