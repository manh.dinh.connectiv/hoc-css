var express = require('express');
var multer  = require('multer');
var router = express.Router();

var controller = require('../controller/userController');
var validate = require('../validate/createRequest');
var middleare = require('../middleware/auth.middleare')
//var middleware = require('../middleware/auth.middleare');
var upload = multer({ dest: './public/uploads/' });



router.get('/', middleare.authMiddleare, controller.index);

//query parameter
router.get('/search', middleare.authMiddleare,controller.search);
// method post
router.get('/registration', controller.create);

router.post('/create', upload.single('avatar'), validate.createMiddleware, controller.postCreate);
//user view
router.get('/:id', middleare.authMiddleare,controller.getById);

module.exports = router;