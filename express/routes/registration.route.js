var express = require('express');

var router = express.Router();
var validate = require('../validate/createRequest');
var controller = require('../controller/registration.controller')

router.get('/', controller.registratiion)
router.post('/', controller.postRegistration)

module.exports = router;