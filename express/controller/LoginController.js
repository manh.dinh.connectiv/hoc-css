var md5 = require('md5');
var User = require('../models/user.model');


module.exports.login = function(req, res) {
    res.render('auth/login');
};

module.exports.postLogin = async function(req, res) {
    var email = req.body.email;
    var password = req.body.password;
    var user = await User.findOne({email: email}).exec();
    if(!user) {
        res.render('auth/login', {
            errors: ['User does not exit'],
            value:req.body
        })
        return;
    }
    var handPassword = md5(password);
    if (user.password !== handPassword) {
        console.log('manh', user.password !== handPassword)
        res.render('auth/login',{
            errors: ['Wrong password'],
            value:req.body
        });
        return;
    }
    res.cookie('userId', user.id,{signed: true});
    res.redirect('/user');
}

