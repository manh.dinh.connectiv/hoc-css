const db = require('../db');
var Sesson = require('../models/session.model')

const Session = require('../models/session.model');
module.exports.appToCart = async function(req, res, next) {

    var productId = req.params.productId;
    var sessionId = req.signedCookies.sessionId;
    var oject = {};
    var productUdate = {};
    search = 'cart.'+productId;
    if(!sessionId) {
        res.redirect('/product');
        return;
    }
    var cart = await Session.find().where(search).gte(1).exec();
    if(cart.length > 0) {
        productUdate['cart.' + productId] = cart[0].cart[productId] + 1;
        await Session.findOneAndUpdate({_id: sessionId}, productUdate, {new: true});
        res.redirect('/product'); 
        return;
    }else{
        oject['cart.'+productId] = 1;
        await Session.findOneAndUpdate({_id: sessionId}, oject, {new: true});
    }
    res.redirect('/product'); 
};
