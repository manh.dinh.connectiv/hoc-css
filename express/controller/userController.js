var shortid = require('shortid');
var db = require('../db');
var User = require('../models/user.model'); 
var users = db.get('users');

module.exports.index = async function(req, res){
    var userMongo = await User.find();
    res.render('user/index', {
        users : userMongo
    })
};

module.exports.search = async function (req, res){
    var q = req.query.q;
    // var userSearch = users.value().filter(function (user) {
    //     return user.name.toLowerCase().indexOf(q.toLowerCase()) !== -1;
    // });
    var userSearch = await User.find({name: {$regex: q}});
   
    res.render('user/index', {
        users: userSearch,
        q : q
    });
};

module.exports.create = function(req, res){
    res.render('user/create');
};

module.exports.postCreate = async function(req, res) {
   
    req.body.id = shortid.generate();
    if(req.file){
        req.body.avatar = req.file.path.split('/').slice(1).join('/');
    }
    var users = await User.create(req.body);
    res.redirect('/login');
};

module.exports.getById = async function(req, res){
    // var userId = parseInt(req.params.id);
    var userId = req.params.id;
    var user = await User.findOne({_id : userId}).exec();
    res.render('user/view', {
        user : user
    })
};
